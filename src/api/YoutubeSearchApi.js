import axios from '../services/axiosService';

const searchVideos = async (search, nextPageToken) => {
    return await axios.get(`/search-videos`, {
        params: {
            search,
            pageToken: nextPageToken
        }
    });
}

export {
    searchVideos
}